---
title: About
layout: page
permalink: "/about/"
---

Hey, I'm Andrew Negri. I am a full stack software engineer with experience
in several different technologies. Currently, I am working on the aftermarket
platform team at GoDaddy in Cambridge, MA where I work on complex distributed
systems that power the largest domain name aftermarket. For more details,
take a look at my linked in.

Outside of my career, I enjoy cocktails, weightlifting, travel, music, Netflix, and video games.