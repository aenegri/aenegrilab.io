---
title: Last of the Oaxacans
layout: post
categories: cocktails
date: '2019-08-27 20:00:00'
---

3/4 oz mezcal  
3/4 oz lime  
3/4 oz maraschino liqueur  
3/4 oz Green Chartreuse

![last-of-the-oaxacans](/assets/img/last-of-the-oaxacans.jpg)

Shake ingredients with ice and pour into a cocktail glass.

After trying the Last Word recently, I was in the mood to try more variations and found the Last of the Oaxacans. The swap of the floral flavors of gin with the smoky flavors of the mezcal  worked better than I anticipated, even in combination with the Green Chartreuse. The aroma is very smoky, with hints of citrus. The body is still quite smoky, but the maraschino, lime, and Green Chartreuse manage to balance it out. During the swallow,  maraschino comes to the forefront.