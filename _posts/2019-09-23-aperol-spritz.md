---
title: Aperol Spritz
layout: post
categories: cocktails
date: '2019-09-23 19:30:00'
---

2 oz Aperol  
3 oz Prosecco  
seltzer

![aperol-spritz](/assets/img/aperol-spritz.jpg)

Add Aperol and Prosecco to glass with ice and top with seltzer. Garnish with orange slice.

The transition from summer to fall has not been smooth, and this past weekend I found myself needing something cool as temperatures went back up. The Aperol Spritz was perfect, and reminded me of why Aperol is great in cocktails. It is a simple drink, starting off with a very fruity orange aroma and a strong citrus and champagne flavor. Although short on complexity, it is very refreshing.