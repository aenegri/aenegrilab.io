---
title: Lessons From My First Year as a Software Engineer
categories: tech
layout: post
date: '2019-06-01 20:00:00'
---

Just last week, three new interns joined my team, reminding me that it has been a year since I graduated college and close to year a since I started working full time. Although the time has gone fast, I have learned much about real development that I simply wasn't exposed to in college.  Here are some of my largest takeaways so far.

## Team Fit is Important

![puzzle](/assets/img/puzzle.jpg)

If you had asked me one year ago about my top priorities when searching for a software engineering job, I would have said salary and technology. However, after spending 40 hours nearly every week at work, I have come to realize that the people you work with with are much more important. Every morning, I come through the door excited to see everyone and talk about what is going on in their lives. During this past spring, each Monday was filled with lively discussions of the Game of Thrones episode from the previous night instead of people sitting at their desks half asleep with a mug of coffee. Although hours each day are spent solitary at a computer, the time with my coworkers always keeps me happy to be in the office, and therefore productive.

## Full Stack is More Than Just Frontend, Backend, and Databases

![servers](/assets/img/network-servers.jpg)

Unless your company has teams dedicated for operational activities, there is a lot more involved in getting an application up and running than just writing the code. In a typical microservice I work on, I have to ensure that metrics are available, create a Jenkins pipeline for automated deployments, configure servers, configure logging, and setup monitoring. Although many of these parts can be copied and pasted from other projects, there are usually minor adjustments to be made which require at least some working knowledge of the technology involved. For instance, now that we are using Golang instead of Java, I had to create new server configurations using SaltStack to ensure that if the service needs to scale, a new server can be up and running quickly. All of this non-code work is very important, and essentially not touched at all in University classroms (or at least it wasn't in mine).

## Solve More Than Just the Problem in Front of You
> "It's easier to ask forgiveness than it is to get permission" - Grace Hopper

This quote often gets bastardized as a way of justifying something bad, but it actually means to go ahead and do something (even without permission) if it is a good idea. This idea doesn't always apply, but there are many times that doing a bit more work than required can really benefit your team. For instance, I have been tasked multiple times with performance testing some key applications using the tool JMeter. Each time I have to do it, I need to spend effort to relearn how my old tests actually worked and reconfigure them if anything has changed. Now, since I have realized this is quite tedious and also leaves me as the only person able to run these tests, I have built a small test runner that anyone can use. Not only does this make my life easier, it makes it so that in the future other coworkers can add their own tests in a central location with several examples. Finding opportunities like this is part of the fun of engineering and everyone will be happy it was done.

## Take Care of Yourself
As we all know, software development is exceptionally sedentary. You show up, sit in a chair at your desk, sit in a chair in a meeting room, and go home. Sure, walking desks exist, but I don't think I've ever seen the one in our office being used. To make matters worse, there are free snacks in the kitchen and lunch delivered from local restaurants. Whoever came up with the freshman 15 needs to come up with a term for the first year in tech. To combat this potentially deadly lifestyle, it is important to take responsibility for you own health.

Personally, I have had much success with exercising before work. This provides an outlet for physical energy and charges you up for the day. I have noticed that some other coworkers like to hit the gym either after work or in the afternoon, and they often end up not being able to make it due to general tiredness from the day or simply having to complete too many tasks. If you can get your body moving before the workday, those conflicts will never happen.

![okonomiyaki](/assets/img/okonomiyaki.jpg)

Another part of taking care of yourself is taking some vacation. As great as your job may be, there are other important things in life like your significant other, family, friends, and other passions. This past April, I was fortunate enough to be able to take some time to travel to Japan with a group of close friends and my girlfriend. I had a wonderful time getting to relax with people I care about while enjoying delicous food and scenery. There were entire days I went without using a laptop. Also, when I got back, I was more ready than ever to crank out some work.

## Conclusion
The first year actually working full time is like trying to drink from a firehose. It is intimidating at first, with unfamiliar norms and expectations and a whole office of people you don't know yet. Over time, though, things start to make more sense and you start to understand what is being discussed at meetings. The most important thing is to just keep pushing through each day to grow as a professional engineer.