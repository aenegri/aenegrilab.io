---
title: The Great Pretender
categories: cocktails
layout: post
date: '2019-08-11 11:00:00'
---

cinnamon-sugar rim  
2 oz Smith & Cross Rum  
1/2 oz pineapple juice  
1/2 oz lime juice  
1/2 oz vanilla syrup  
1 teaspoon cinnamon bark syrup

![the-great-pretender](/assets/img/the-great-pretender.jpg)

Rim a coupe with cinnamon-sugar. To wet the rim, I used a bit of leftover lime juice. Then shake ingredients with ice and pour.

Two days ago, I was flipping through Death & Co and wanted to expand past the classic cocktails section. I landed on The Great Pretender since it requires a bit more ingredient prep, but is still less labor intense than many other options. The strong flavors of the over proof rum come through well and the booziness is tamed by the sweetness of the other ingredients