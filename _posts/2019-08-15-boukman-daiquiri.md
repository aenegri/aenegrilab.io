---
title: Boukman Daiquiri
categories: cocktails
layout: post
date: '2019-08-15 20:00:00'
---

1 1/2 oz white rum  
1/2 oz cognac  
3/4 oz lime juice  
3/4 oz cinnamon syrup  

![boukman-daiquiri](/assets/img/boukman-daiquiri.jpg)

Shake with ice and strain into a coupe. Optionally garnish with lime.

After making The Great Pretender, I searched around the internet for other drinks that use cinnamon syrup. I came across the Boukman Daiquiri from imbibe magazine, and it actually turned out quite good. The rum, lime, and cinnamon create a flavor profile not that different from a regular daiquiri with a bit of spice, and the cognac comes through on the tail end.