---
title: The Long Road on Advent of Code
categories: tech adventofcode
layout: post
date: '2018-12-13 12:13:00'
---

Keeping up with [Advent of Code][advent-of-code] has proven to be quite the challenge. Solving one question a day didn't sound too bad, but real life caught up with me last week. After missing two consecutive days, I was trapped like a cartoon character in quicksand. In a desperate effort to escape my plight, I completed three days worth of challenges on Saturday and another three on Sunday to finally be caught up.

Coming into day thirteen, it was time to make an attempt on entering the leaderboards. With high hopes and low expectations, I prepared my editor with all the boilerplate in place. I knew my chances of a high ranking were slim, as a dedicated elite group had solved most of the previous questions in mere *minutes*. Nevertheless, I gave it a shot. 

![meme](/assets/img/coding-fast-meme.jpg)

The two hours I spent quickly became a blur. Although the problem wasn't too bad, I ended up making several minor mistakes that were entirely related to my lack of knowledge on JavaScript arrays. For instance, I did not realize that using `delete` to remove an array entry would not shift over the remaining items like an ArrayList in Java would when calling `list.remove(i)`. Eventually, I received my coveted two gold stars for the night at nearly two in the morning.

Needless to say, I didn't quite make the top 100. To be specific, I landed at spot 763 with a time of `01:20:00` for part one and spot 686 with a time of `01:48:20` for part two. It was fun, but I don't plan on sacrificing any more sleep for the rest of the event. Instead, I plan to utilize it as interview prep with the following goals in mind:

* Write legible code. In an actual workplace, this is more important than hyper optimizing or shipping something a little bit earlier.
* Learn more JavaScript. Previously, I had performed all my interviews in Java and I want to change that. However, that was not something I could realistically do without significant time investment to learn the nuances of arrays, sets, and other data structures.
* Become faster. I'm not sure if I'll reach a level where I can finish in under 10 minutes, but if I can get under an hour I would be meeting expectations for an interview.

I don't plan on leaving my job anytime soon, but it is important to keep these skills up so the next time around doesn't require multiple months of grinding technical questions. If anyone is curious about my code, feel free to [take a look][aoc-repo]. I purposely left it ugly for day thirteen to show what actually got me to my answers.

[advent-of-code]: https://adventofcode.com/
[aoc-repo]: https://gitlab.com/aenegri/advent-of-code-2018