---
title: Orange Vanilla Old Fashioned
categories: cocktails
date: '2019-08-20 21:30:00'
layout: post
---

2 oz bourbon  
1/2 oz vanilla syrup  
1 dash Angostura bitters  
1 dash orange bitters  
orange slice  
orange twist

![orange-vanilla-old-fashioned](/assets/img/orange-vanilla-old-fashioned.jpg)

In  a shaker, muddle the syrup and orange slice. Add the other liquids and shake with ice, and strain into a double Old Fashioned glass with one large ice cube. Finish off by expressing  the orange twist over the drink and dropping it in.

Last week I was inspired by a [post on the cocktails subreddit](https://www.reddit.com/r/cocktails/comments/cqyuth/made_a_vanilla_and_blood_orange_old_fashioned/) featuring a blood orange and vanilla Old Fashioned. The original recipe seemed a bit on the sweet side, so I reduced the sugar and introduced some bitters. The flavour profile is primarily bourbon complemented by orange. The vanilla sits in the background, offering a gentle creamy backdrop and the bitters prevent it from tasting too sweet.