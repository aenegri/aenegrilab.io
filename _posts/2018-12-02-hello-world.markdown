---
layout: post
title:  "Hello, World!"
date:   2018-12-02 09:24:00 -0700
categories: meta update
sharing:
    facebook: "Hello, World!"
---
Welcome to my personal website! I've been meaning to do something like this
for quite a while, but am just now getting around to it. The plan for this
is to be a personal online space that is separate from social media, which
I am getting more and more tired of. I plan to focus on tech and workplace
related topics, but may venture into other interests as well.

Creating this was actually exceptionally straightforward and could be done
with minimal technical knowledge. I simply followed the directions in this
[gitlab repository][gitlab-jekyll] and configured DNS for my domain
`andrewnegri.com`. If you're curious, here is the [source][gitlab-aenegri].

[gitlab-jekyll]: https://gitlab.com/pages/jekyll
[gitlab-aenegri]: https://gitlab.com/aenegri/aenegri.gitlab.io