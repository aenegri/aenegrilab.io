---
title: Division Bell
layout: post
categories: cocktails
date: '2019-08-24 20:30:00'
---

1 oz mezcal  
3/4 oz Aperol  
3/4 oz lime  
1/2 oz maraschino liqueur

![division-bell](/assets/img/division-bell.jpg)

Shake ingredients with ice and pour into coupe. Garnish with a grapefruit twist.

Today I was in the mood for something with mezcal that is also a bit on the lighter side, so I decided to try one of Phil Ward's Last Word style drinks. The aroma is a mixture of smoke and citrus, and so is the body. When swallowing, things get a bit drier as the maraschino comes to the forefront.